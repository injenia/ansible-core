﻿# .EXAMPLE
#     # upgrade to powershell 4.0 with automatic login and reboots
#     powershell.exe -ExecutionPolicy ByPass -File win_gce_ansible.ps1 -Verbose

Param(
    [string]$version = "5.1",
    [string]$username,
    [string]$password,
    [switch]$verbose = $false
)
$ErrorActionPreference = 'Stop'
if ($verbose) {
    $VerbosePreference = "Continue"
}
$tmp_dir = $env:temp
if (-not (Test-Path -Path $tmp_dir)) {
    New-Item -Path $tmp_dir -ItemType Directory > $null
}

Function Write-Log($message, $level="INFO") {
    # Poor man's implementation of Log4Net
    $date_stamp = Get-Date -Format s
    $log_entry = "$date_stamp - $level - $message"
    $log_file = "$tmp_dir\upgrade_powershell.log"
    Write-Verbose -Message $log_entry
    Add-Content -Path $log_file -Value $log_entry
}

Function Reboot-AndResume($auto_start = $true) {
    if ($auto_start) {
        Write-Log -message "Configuring script to auto-run on next logon"
        $script_path = $script:MyInvocation.MyCommand.Path
        $ps_path = "$env:SystemDrive\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
        $arguments = "-version $version"
        if ($username -and $password) {
            $arguments = "$arguments -username `"$username`" -password `"$password`""
        }
        if ($verbose) {
            $arguments = "$arguments -Verbose"
        }

        $command = "$ps_path -ExecutionPolicy ByPass -File $script_path $arguments"
        $reg_key = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"
        $reg_property_name = "ps-upgrade"
        Set-ItemProperty -Path $reg_key -Name $reg_property_name -Value $command

        if ($username -and $password) {
            $reg_winlogon_path = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
            Set-ItemProperty -Path $reg_winlogon_path -Name AutoAdminLogon -Value 1
            Set-ItemProperty -Path $reg_winlogon_path -Name DefaultUserName -Value $username
            Set-ItemProperty -Path $reg_winlogon_path -Name DefaultPassword -Value $password
            Write-Log -message "Rebooting server to continue powershell upgrade..."
        } else {
            Write-Log -message "A reboot is needed to continue powershell upgrade."
            $reboot_confirmation = Read-Host -Prompt "A reboot is needed to continue powershell upgrade, do you wish to proceed? (y/n)"
            if ($reboot_confirmation -ne "y") {
                $error_msg = "Please reboot server manually and login to continue upgrade process, the script will restart on the next login automatically."
                Write-Log -message $error_msg -level "ERROR"
                throw $error_msg
            }
        }
    }
    else {
        Write-Log -message "-"
        Write-Log -message "Press any key to reboot..."
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
    }

    if (Get-Command -Name Restart-Computer -ErrorAction SilentlyContinue) {
        Restart-Computer -Force
    } else {
        # PS v1 (Server 2008) doesn't have the cmdlet Restart-Computer, use el-traditional
        shutdown /r /t 0
    }
}

Function Clear-AutoLogon {
    $reg_winlogon_path = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
    Write-Log -message "Clearing auto logon registry properties..."
    Set-ItemProperty -Path $reg_winlogon_path -Name AutoAdminLogon -Value 0
    Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultUserName -ErrorAction SilentlyContinue
    Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultPassword -ErrorAction SilentlyContinue
}

Function Download-File($url, $path) {
    $filename  = $url.Split("/")[-1]
    Write-Log -message "Downloading $filename..."
    $client = New-Object -TypeName System.Net.WebClient
    $client.DownloadFile($url, $path)
}

Function Run-Process($executable, $arguments) {
    $process = New-Object -TypeName System.Diagnostics.Process
    $psi = $process.StartInfo
    $psi.FileName = $executable
    $psi.Arguments = $arguments
    Write-Log -message "Executing '$executable $arguments'..."
    $process.Start() | Out-Null
    
    $process.WaitForExit() | Out-Null
    $exit_code = $process.ExitCode
    Write-Log -message "Process completed with exit code: $exit_code."

    return $exit_code
}

#Adapted from https://www.jonathanmedd.net/2014/02/testing-for-the-presence-of-a-registry-key-and-value.html
function Test-RegistryValue {
    param (
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]$Path,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]$Name
    )
    try {
        Get-ItemProperty -Path $Path | Select-Object -ExpandProperty $Name -ErrorAction Stop | Out-Null
        return $true
    }
    catch {
        return $false
    }
}

#Adapted from https://gist.github.com/altrive/5329377
#Based on <http://gallery.technet.microsoft.com/scriptcenter/Get-PendingReboot-Query-bdb79542>
function Test-PendingReboot
{
 if (Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending" -EA SilentlyContinue) { return $true }
 if (Get-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired" -EA SilentlyContinue) { return $true }
 if (Get-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager" -Name PendingFileRenameOperations -EA SilentlyContinue) { return $true }
 try { 
   $util = [wmiclass]"\\.\root\ccm\clientsdk:CCM_ClientUtilities"
   $status = $util.DetermineIfRebootPending()
   if(($status -ne $null) -and $status.RebootPending){
     return $true
   }
 }catch{}
 
 return $false
}

if (Test-PendingReboot) {
    Write-Log -message "Pending reboot detected, continue..."
    Write-Log -message "-"
    Reboot-AndResume
}

Write-Log -message "Starting OS configuration for Google GCE..."
Write-Log -message "-------------------------------------------"
$os_version = [System.Environment]::OSVersion.Version
if ($os_version -lt [version]"6.1.7600") {
    # On WS2008R2, upgrade to SP1 and then proceed
    Write-Log -message "-"
    Write-Log -message "Windows Server OS prior to release 2008 R2 are not supported."
    Write-Log -message "-"
    Write-Log -message "Cleaning system registry and resetting security policies..."
    # this isn't needed but is a good security practice to complete
    Set-ExecutionPolicy -ExecutionPolicy Undefined -Force
    Clear-AutoLogon
    exit 50
}
elseif ($os_version -ge [version]"6.1.7601") {
    Write-Log -message "-"
    Write-Log -message "Supported Windows Server OS version found, proceeding with configuration..."
    
    if ($os_version -ge [version]"6.1.7601" -and $os_version -lt [version]"6.2") {
        Write-Log -message "-"
        Write-Log -message "Configuring Windows Server 2008 R2 SP1 for using TLS 1.2..."
        # Installing TLS 1.2 Hotfix
        if ( !(Get-HotFix -Id "KB3080079" -ErrorAction SilentlyContinue) ) {
            Write-Log -message "-"
            $url       = "https://download.microsoft.com/download/F/4/1/F4154AD2-2119-48B4-BF99-CC15F68E110D/Windows6.1-KB3080079-x64.msu"
            $error_msg = "Failed to apply TLS 1.2 Hotfix for Windows Server 2008 R2 SP1."
            $arguments = "/quiet /norestart"
            $filename  = $url.Split("/")[-1]
            $file      = "$tmp_dir\$filename"
            Download-File -url $url -path $file
            $exit_code = Run-Process -executable $file -arguments $arguments
            if ($exit_code -ne 0 -and $exit_code -ne 3010 -and $exit_code -ne -2145124329) {
                Write-Log -message $error_msg -level "ERROR"
                throw $error_msg
            }
            if ($exit_code -eq 3010) {
                Reboot-AndResume
            }
        }
        # Patching registry for the use of TLS 1.2 in WinRM
        $reg_path = "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2"
        if ( !(Test-Path -Path $reg_path ) )          { New-Item -Path $reg_path }
        if ( !(Test-Path -Path "$reg_path\Server" ) ) { New-Item -Path "$reg_path\Server" }
        if ( !(Test-Path -Path "$reg_path\Client" ) ) { New-Item -Path "$reg_path\Client" }
        if ( !(Test-RegistryValue -Path "$reg_path\Server" -Name "Enabled") ) {
            New-ItemProperty -Path "$reg_path\Server" -Name "Enabled" -Value 1 -PropertyType DWord
        } else {
            Set-ItemProperty -Path "$reg_path\Server" -Name "Enabled" -Value 1
        }
        if ( !(Test-RegistryValue -Path "$reg_path\Server" -Name "DisabledByDefault") ) {
            New-ItemProperty -Path "$reg_path\Server" -Name "DisabledByDefault" -Value 0 -PropertyType DWord
        } else {
            Set-ItemProperty -Path "$reg_path\Server" -Name "DisabledByDefault" -Value 0
        }
        if ( !(Test-RegistryValue -Path "$reg_path\Client" -Name "Enabled") ) {
            New-ItemProperty -Path "$reg_path\Client" -Name "Enabled" -Value 1 -PropertyType DWord
        } else {
            Set-ItemProperty -Path "$reg_path\Client" -Name "Enabled" -Value 1
        }
        if ( !(Test-RegistryValue -Path "$reg_path\Client" -Name "DisabledByDefault") ) {
            New-ItemProperty -Path "$reg_path\Client" -Name "DisabledByDefault" -Value 0 -PropertyType DWord
        } else {
            Set-ItemProperty -Path "$reg_path\Client" -Name "DisabledByDefault" -Value 0
        }
    }
    
    Write-Log -message "-"
    $url       = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1"
    $error_msg = "Powershell upgrade has failed."
    $arguments = "-version $version"
    if ($username -And $password) {
        $arguments = "$arguments -username `"$username`" -password `"$password`""
    }
    if ($verbose) {
        $arguments = "$arguments -Verbose"
    }
    $filename  = $url.Split("/")[-1]
    $file      = "$tmp_dir\$filename"
    Download-File -url $url -path $file
    
    Write-Log -message "-"
    Write-Log -message "Starting Powershell upgrade..."
    Write-Log -message "-------------------------------------------"
    $process   = Start-Process powershell -Argument "$file $arguments" -NoNewWindow -PassThru -Wait
    $process.WaitForExit()
    $exit_code = $process.ExitCode
    if ($exit_code -ne 0 -and $exit_code -ne 3010) {
        Write-Log -message "$error_msg $exit_code" -level "ERROR"
        throw "$error_msg $exit_code"
    }
    Write-Log -message "-------------------------------------------"
    Write-Log -message "Powershell upgrade completed, continue..."

    Write-Log -message "-"
    $url       = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
    $error_msg = "WinRM configuration has failed."
    $arguments = "-CertValidityDays 3650 -ForceNewSSLCert -EnableCredSSP -DisableBasicAuth -SkipNetworkProfileCheck"
    if ($verbose) {
        $arguments = "$arguments -Verbose"
    }
    $filename  = $url.Split("/")[-1]
    $file      = "$tmp_dir\$filename"
    Download-File -url $url -path $file

    Write-Log -message "-"
    Write-Log -message "Starting WinRM configuration..."
    Write-Log -message "-------------------------------------------"
    $process   = Start-Process powershell -Argument "$file $arguments" -NoNewWindow -PassThru -Wait
    $process.WaitForExit()
    $exit_code = $process.ExitCode
    if ($exit_code -ne 0 -and $exit_code -ne 3010) {
        Write-Log -message "$error_msg $exit_code" -level "ERROR"
        throw "$error_msg $exit_code"
    }
    Write-Log -message "-------------------------------------------"
    Write-Log -message "WinRM configuration completed, continue..."

    Write-Log -message "-"
    Write-Log -message "Configuring OS for GCE..."
    Write-Log -message "-------------------------------------------"

    Write-Log -message "-"
    Write-Log -message "Enabling Remote Desktop Protocol..."
    $sys_lang  = (Get-Culture).LCID
    $rdp_rule  = if ($sys_lang -eq 1040 -or $sys_lang -eq 2064) {"Desktop Remoto"} else {"Remote Desktop"}
    Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Terminal Server" -Name "fDenyTSConnections" -Value 0
    netsh advfirewall firewall set rule group="$rdp_rule" new enable=Yes
    
    Write-Log -message "Disabling Windows Firewall..."
    netsh advfirewall set allprofiles state off
    
    Write-Log -message "Turning on Emergency Management Services..."
    #bootcfg /ems ON /port COM2 /baud 115200 /id 1
    bcdedit /emssettings EMSPORT:2 EMSBAUDRATE:115200
    bcdedit /ems on

    Write-Log -message "Enabling the SOS boot setting..."
    #bootcfg /addsw /SO /ID 1
    bcdedit /set bootlog yes bootstatuspolicy DisplayAllFailures

    Write-Log -message "Installing Windows Guest Environment for GCE..."
    Write-Log -message "-"
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $url       = "https://github.com/google/googet/releases/download/v2.13.0/googet.exe"
    $error_msg = "GooGet setup has failed."
    $arguments = "-root C:\ProgramData\GooGet -noconfirm install -sources https://packages.cloud.google.com/yuck/repos/google-compute-engine-stable googet"
    $filename  = $url.Split("/")[-1]
    $file      = "$tmp_dir\$filename"
    Download-File -url $url -path $file
    $exit_code = Run-Process -executable $file -arguments $arguments
    if ($exit_code -ne 0 -and $exit_code -ne 3010 -and $exit_code -ne -2145124329) {
        Write-Log -message $error_msg -level "ERROR"
        throw $error_msg
    }
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
    $env:GooGetRoot = [System.Environment]::GetEnvironmentVariable("GooGetRoot","Machine")
    googet addrepo google-compute-engine-stable https://packages.cloud.google.com/yuck/repos/google-compute-engine-stable
    googet -noconfirm install google-compute-engine-windows google-compute-engine-sysprep google-compute-engine-metadata-scripts google-compute-engine-vss
    googet -noconfirm install google-compute-engine-auto-updater

    Clear-AutoLogon
    Write-Log -message "Resetting security policies..."
    # this isn't needed but is a good security practice to complete
    Set-ExecutionPolicy -ExecutionPolicy Undefined -Force

    Write-Log -message "-"
    Write-Log -message "Process complete. You should run Set-ExecutionPolicy -ExecutionPolicy Restricted -Force"
    Reboot-AndResume($false)
}
else {
    Write-Log -message "-"
    Write-Log -message "Windows Server 2008 R2 SP0 found, installation of SP1 is required, proceeding with setup..."
    $url       = "https://download.microsoft.com/download/0/A/F/0AFB5316-3062-494A-AB78-7FB0D4461357/windows6.1-KB976932-X64.exe"
    $error_msg = "Failed to update Windows Server 2008 R2 to SP1."
    $arguments = "/quiet /norestart"
    $filename  = $url.Split("/")[-1]
    $file      = "$tmp_dir\$filename"
    Download-File -url $url -path $file
    $exit_code = Run-Process -executable $file -arguments $arguments
    if ($exit_code -ne 0 -and $exit_code -ne 3010) {
        Write-Log -message $error_msg -level "ERROR"
        throw $error_msg
    }
    Reboot-AndResume
}