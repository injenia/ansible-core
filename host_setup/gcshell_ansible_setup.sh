#!/bin/bash

gce_inst=ansible-master
gce_size=f1-micro
gce_zone=europe-west1-b

gcloud config set compute/zone $gce_zone

gcloud iam service-accounts create ansible --display-name="Ansible service account" --quiet
gcloud iam service-accounts keys create ~/$DEVSHELL_PROJECT_ID.json --iam-account ansible@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --quiet
gcloud projects add-iam-policy-binding $DEVSHELL_PROJECT_ID --member serviceAccount:ansible@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --role roles/editor --quiet

gcloud compute instances create $gce_inst --machine-type=$gce_size --zone=europe-west1-b --service-account=ansible@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --scopes=https://www.googleapis.com/auth/cloud-platform --quiet
sleep 5s

gcloud compute ssh ansible-master --quiet << HERE
  wget https://gitlab.com/mpellin/ansible-core/raw/devel/host_setup/ansible_master_setup.sh -O ~/ansible_master_setup.sh
  sudo chmod +x ~/ansible_master_setup.sh
  sudo ~/ansible_master_setup.sh
HERE

gcloud compute scp ~/$DEVSHELL_PROJECT_ID.json $gce_inst:/tmp --quiet
gcloud compute ssh ansible-master --quiet << HERE
  sudo mv /tmp/$DEVSHELL_PROJECT_ID.json /root/
  sudo chmod 400 /root/$DEVSHELL_PROJECT_ID.json
HERE
rm -f ~/$DEVSHELL_PROJECT_ID.json

gcloud compute ssh ansible-master --quiet << HERE
  sudo cp /root/.ssh/ansible.pub ~/
  sudo chmod 777 ~/ansible.pub
HERE
gcloud compute scp $gce_inst:~/ansible.pub ~/ --quiet
gcloud compute ssh ansible-master --command="sudo rm -f ~/ansible.pub" --quiet

echo -n "ansible:" > ~/new_ssh_metadata && cat ansible.pub >> ~/new_ssh_metadata
gcloud compute project-info describe --format=json | jq -r '.commonInstanceMetadata.items[] | select(.key == "ssh-keys") | .value' >> new_ssh_metadata
cd ~/
gcloud compute project-info add-metadata --metadata-from-file ssh-keys=new_ssh_metadata --quiet
rm -f ~/new_ssh_metadata
rm -f ~/ansible.pub

sleep 3s
gcloud compute ssh ansible-master --command="sudo ansible -i /usr/local/ansible/inventory/$DEVSHELL_PROJECT_ID status_running -m ping"