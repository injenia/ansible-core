#!/bin/bash

project_id=$(dnsdomainname | grep -oP "^c.\K([a-z\-]+)(?=.+$)")

echo ""
echo "Adding Ansible repository to sources..."
echo "---------------------------------------"
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" | sudo tee -a /etc/apt/sources.list
apt-get install -y dirmngr && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367

echo ""
echo "Installing Ansible..."
echo "---------------------------------------"
apt-get update && apt-get install -y ansible python-pip
yes | pip install apache-libcloud pytz

echo ""
echo "Generating new SSH Keys..."
echo "---------------------------------------"
yes y | ssh-keygen -t rsa -C ansible -f /root/.ssh/ansible -N '' -q

echo ""
echo "Editing ansibles's config file..."
echo "---------------------------------------"
sed -i -e "s+#host_key_checking+host_key_checking+g" /etc/ansible/ansible.cfg
sed -i -e "s+#remote_user = root+remote_user = ansible+g" /etc/ansible/ansible.cfg
sed -i -e "s+#private_key_file = /path/to/file+private_key_file = /root/.ssh/ansible+g" /etc/ansible/ansible.cfg

echo ""
echo "Creating GCE dynamic inventory..."
echo "---------------------------------------"
mkdir -p /usr/local/ansible/inventory/$project_id
wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/gce.py -O /usr/local/ansible/inventory/$project_id/gce.py
wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/gce.ini -O /usr/local/ansible/inventory/$project_id/gce.ini
chmod +x /usr/local/ansible/inventory/$project_id/gce.py
sed -i -e "s+gce_project_id =+gce_project_id = $project_id+g" /usr/local/ansible/inventory/$project_id/gce.ini
sed -i -e "s+gce_zone =+#gce_zone =+g" /usr/local/ansible/inventory/$project_id/gce.ini
sed -i -e "s+cache_max_age = 300+cache_max_age = 0+g" /usr/local/ansible/inventory/$project_id/gce.ini
