# Containerizing Ansible jobs

<img src="https://raw.githubusercontent.com/fabric8io/kansible/master/docs/images/logo.png" height="200" width="200" alt="kansible logo"/>

The goal of this project is to use ansible processes as Kubernetes CronJobs, allowing CI/CD with an automatizated pipeline, from the release of a playbook commit on Git and down to the Ansible container. Here are some pros of this solution:

  - **Reliability.** No need to mantain Ansible master(s) thanks to Kubernets structure of pods and clusters.
  - **Consistency.** Thanks to containerization the Ansible environment is static within its version and easy to implement.
  - **Versioning.** The Ansible enviroment is now containerized and therefore binded to its release version.
  - **Descriptible and easy to update.** With a properly configurated CI/CD process your jobs will always use the last release of your playbooks, and you can always go back to a working environment just in case an update isn't working as expected.

## Why Kubernetes?
Beside orchestrating your Ansible Jobs and solving the ol' fashioned infrastructure problems, K8s come with a very useful set o features you can use to better manage the security and the implementation of your playbooks.
As you probably know, working with Ansible and Google Cloud requires managing dynamic inventories and therefore the security of sensitive data related to your projects.

Here is where **[Kubernetes secret volumes](https://kubernetes.io/docs/concepts/configuration/secret/)** come in handy.
You can use them to safely archive and use your sensitive data on your Ansible container, mounting them as volumes, or dynamically mount single files as those used by Ansible inventory variables.

### Example of using secret volumes
```yaml
[...]
          volumes:
          - name: my-secret-volume
            secret:
              secretName: my-secret-volume
              defaultMode: 256
              items:
              - key: gce.py
                path: gce.py
                mode: 320
              - key: gce.ini
                path: gce.ini
              - key: all
                path: group_vars/all
              - key: ssh-file
                path: .auth/ssh-file
              - key: auth-file.json
                path: .auth/auth-file.json
[...]
```

## How to generalize your playbooks
The challenge of creating an Ansible container packaged with all your beautiful playbooks that works across multiple projects and platforms requires quite a bit of code adaptation and forethinking.

### Retrieve credentials dynamically
First things first: most if not all Ansible modules for Google Cloud requires the right credentials to be correctly executed which are, surprise, the same used by your dynamic inventory. So, here is our solution for retrieving the credental file path from the inventory file:

```yaml
- hosts: all

  vars:
    ini_file: "{{ inventory_file | regex_replace('py', 'ini') }}"
    gce_proj: "{{ lookup('ini', 'gce_project_id section=gce file={{ ini_file }}') }}"
    srv_acct: "{{ lookup('ini', 'gce_service_account_email_address section=gce file={{ ini_file }}') }}"
    crd_file: "{{ lookup('ini', 'gce_service_account_pem_file_path section=gce file={{ ini_file }}') }}"

[...]
```

Now on we can use those variables to fuel all the Google Cloud modules in our playbooks.

### Accept command line parameters and defaults
The limit of containerization is that our environment is static within a specific release.
Considering that and the fact that our playbooks have to be as generic as possible in order to avoid mantaining multiple variants of the same code, the only option we have to apply small changes (as backup retention days for example) is passing them through the *--extra-vars* Ansible parameter:

>ansible-playbook -i <inventory> -l <tags> your_playbook.yml --extra-vars 'var_one=test1 var_two=test2 [...]'

We then need to accept those variables inside our playbook, possibly applying some default values:
```yaml
- hosts: all

  vars:
    bkp_days: "{{ retention_days | default(0) }}"
    bkp_cops: "{{ retention_copies | default(0) }}"
    bkp_prfx: "{{ backup_prefix | default('ansbkp') }}"

[...]
```
In case defaults are not appliable, you should verify the presence of all needed variables, and eventually fail:
```yaml
[...]

  tasks:
      - name: Verifying if all necessary variables are set...
        fail:
          msg: "Variable {{item}} has not been set"
        when: item not in vars
        with_items:
          - var_1
          - var_2
          - var_3

[...]
```

### Available modules:
As for now, you can use those modules:

#### Backup module
Execute snapshots of specified (or all) machine(s) disks.

Extra parameters:
* retention_days (default 0)
* retention_copies (default 0)
* backup_prefix (default 'ansbkp')
Positive values sets the retention, 0 ignore the parameter and -1 clears all the copies of that disk with the selected prefix.

#### GCE system update module
This module updates the following OS:
* Debian
* Ubuntu
* Windows

with the latest security updates, and is able to perform functionality tests through the use of *tst_task* extra variable:
* SuiteCRM (suitecrm)
* Zucchetti Infinity CRM (infinity-hr)

##### SuiteCRM Test
Extra parameters (all mandatory, no defaults):
* test_path - CRM URL published path (Ex. suitecrm.com/test_path)
* test_user - CRM login user
* test_pass - CRM login password

##### Zucchetti Infinity CRM Test
Just a placeholder with a *win_ping* call at the moment.

#### Notifications
Each of those modules notifies on failures through SendGrid.
You can use this list of parameters to setup the notification alerts:
* sendgrid_user - SendGrid login user
* sendgrid_pass - SendGrid login password
* sendgrid_mssg - SendGrid notification mail subject
* sendgrid_dest - SendGrid notification mail recipient

## New possibilities: CI/CD
Now that we have implemented our Ansible playbooks as Kubernetes CronJobs, we are opened to a new scenario: creating a continuos integration/delivery pipeline with some help from open-source tools such as **[Jenkins](https://jenkins.io/)** (or **[Jenkins X](https://jenkins.io/projects/jenkins-x/)** for a more cloud-native solution on K8s).

### The big picture
![Ansible CI/CD Schema](https://docs.google.com/drawings/d/e/2PACX-1vQiDlfDkce1NqFkkRPx1KAyS4hQWioBfta4ErUsXiiMNp3BLHBOaTGd4vCQ8MPuROoXqHon-w9IBnDY/pub?w=1139&h=720)
This is the complete process:

* A developer pushes the new release of a playbook in the Git repository
* GitLab intercepts the new commit and sends a call to Jenkins webhook
* Jenkins starts building the Dockerfile along with your changes
* The updated image will be tagged and pushed on Google Container Registry
* On the next execution, Ansible CronJob will pull the last image thanks to the right policy
* The Ansible Job will connect to the appropriate inventory with the credential stored on the secret mount

## Our module modifications
While Ansible is constantly developing new modules and improving the existing ones, we found limitations in some modules we are using, and we added the necessary modifications.

### GCE Snapshot module
We added retention capabilities (copies, days) to this module, nothing was changed of the original logic. Not providing the new parameters means that our module will act exactly as the vanilla one.
All the changes will act before the original module, so you can use the module for both cleaning the snapshot history AND taking a new one, for example.

## Changelog
#### v1.2.0
* Dockerfile refactoring
* Using pip instead of apt as base for setup
* Developed Zucchetti Infinity tests for DB and FE

#### v1.1.2
* Documentation update

#### v1.1.1
* Fix ansible's dependencies for SSH connections

#### v1.1.0
* Added support for Windows Updates on gce-os-update playbook
* Major structure refactoring, now all playbooks and variables are modular

#### v1.0.2
* Fixed Dockerfile build (Python Wheel package)

#### v1.0.1
* Fixed Dockerfile build (ca-certificates package)

#### v1.0.0
* Initial release: support for gce-os-update and gce-snapshot-backup playbooks
